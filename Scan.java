
import java.io.FileNotFoundException;
import java.io.File;
import java.util.Scanner;

class Scan {
	
	private int line;
	private int col;
	private int index;
	private Scanner input;
	private String currentLine;
	private boolean end = false;
	
	
	 // constructor for scan
	  
	public Scan(String file) throws FileNotFoundException {
		if (file == null) {
			throw new IllegalArgumentException("Null file name.");
		}
		
		input = new Scanner(new File("input.scl"));
		setLine();
		line = 1;
		col = 0;
		index = 0;
		
	}
	
	 // skipping whitespace before the next lexeme
	// checks if the next character is whitespace, if it is whitespace it increases the index to the next character
	public void skipSpace() {
		while(index < currentLine.length() && Character.isWhitespace(currentLine.charAt(index))) {
			index++;
		}
	}
	
	// skips white space and proceeds through the characters to create a lexeme and return it
	
	public String getLexeme() {
		String lexeme = "";
		skipSpace();
		
		//check to see if we are at the last character of a line, if we are we go to the next line
		if (index ==currentLine.length()) {
			index = 0;
			setLine();
		}
		
		col = index + 1; //sets the column number
		
		// a while loop that goes through each character and creates a lexeme until it hits white space.
		while (index < currentLine.length() 
				&& !Character.isWhitespace(currentLine.charAt(index))) {
			lexeme = lexeme + currentLine.charAt(index);
			index++;
		}
		
		if(end) {
			return "endoffile";
		}
		else {  
			return lexeme;
		}
		
	}  
	
	// Sets the line
	
	private void setLine() {
		if (input.hasNext()) {
			currentLine = input.nextLine();
			skipSpace();
			
			if(currentLine.isEmpty()) {
				setLine();
				
			}
			
		}
		  // an else that says that if the line is empty and there is no next, that we must be at the end of the file
		else {
			end = true;
		}

		line++;
			
	}
	
	//Getters for Line and column
	
	public int getLine() {
		return this.line;
	}
	
	public int getCol() {
		return this.col;
	}
}